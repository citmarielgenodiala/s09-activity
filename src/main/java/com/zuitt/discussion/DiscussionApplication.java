package com.zuitt.discussion;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// Will require all routes within the "Discussion Application" to use "/greeting" as part of its routes
@RequestMapping("/greeting")
//The "@RestController annotation" tells Spring Boot that this application will function as an endpoint that will be used in handling web requests
public class DiscussionApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hello() {
		return "Hello World!";
	}

	// Routes with a string query
	// Dynamic data is obtained from the URL's string query
	// "%s" specifies that the value to be included in the format is of any data type
	// http://localhost:8080/hi?name=Joe

	@GetMapping("/hi")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	// Multiple Parameters
	// http://localhost:8080/friend?name=Ash&friend=Pikachu
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	// Routes with path variables
	@GetMapping("/hello/{name}")
	// http://localhost:8080/hello/joe
	// @PathVariable annotation allows us to extract data directly from the URL
	public String courses (@PathVariable ("name") String name){
		return String.format("Nice to meet you %s!", name);
	}


	// Assignment s09
	// #1
	ArrayList<String> enrollees = new ArrayList<String>();

	// #2
	// ex: http://localhost:8080/greeting/enroll?user=John
	@GetMapping("/enroll")
	public String addEnrolles(@RequestParam(name = "user") String user){
		enrollees.add(user);
//		return "Welcome " + user + "!";
		return "Thank for enrolling, " + user + "!";
	}

	// #3
	//http://localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

	// #4

	@GetMapping("/nameage")
	// ex: http://localhost:8080/greeting/nameage?name=Jil&age=18
	public String nameAge(@RequestParam(name = "name") String name, @RequestParam(name = "age") int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	// #5
	@GetMapping("/courses/{id}")
	// ex: http://localhost:8080/greeting/courses/javaee101
	public String getCourses(@PathVariable String id) {
		String courseInfo;
		//Since no specific example was provided for the schedule and price, I have created my own.
		//Personally, I prefer using the switch case method over the If-else statement.
		switch(id) {
			case "java101":
				courseInfo = "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
				break;
			case "sql101":
				courseInfo = "Name: SQL 101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 2500";
				break;
			case "javaee101":
				courseInfo = "Name: JavaEE 101, Schedule: THF 1:30 PM - 4:00 PM, Price: PHP 3500";
				break;
			default:
				courseInfo = "Course cannot be found.";
				break;
		}
		return courseInfo;
	}




}
